package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/antonkri97/Slivky/models"
	"github.com/antonkri97/vapeserver/db"
	"github.com/antonkri97/vapeserver/jwt"
	mgo "gopkg.in/mgo.v2"
)

// Login create password
// pass - BYMvqNH7CvnTTMsLaETxDYnCedDLo
func Login(w http.ResponseWriter, r *http.Request) {
	admin := models.Admin{}
	if err := json.NewDecoder(r.Body).Decode(&admin); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Printf("login is %s\npassword is %s", admin.Username, admin.Password)

	database := r.Context().Value("db").(*mgo.Database)
	err := db.ValidateAdmin(database, admin.Username, []byte(admin.Password))
	if err != nil {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	w.Write([]byte(jwt.GenerateToken()))
	w.Header().Set("Content-Type", "text/plain")
}
