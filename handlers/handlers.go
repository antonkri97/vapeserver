package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/antonkri97/vapeserver/db"
	"github.com/antonkri97/vapeserver/models"
	"github.com/pressly/chi"
	mgo "gopkg.in/mgo.v2"
)

// GetItems finds all the items and sends json
func GetItems(w http.ResponseWriter, r *http.Request) {
	vape := r.Context().Value("db").(*mgo.Database)

	js, err := db.GetItems(vape)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

// AddItem take json item and insert it to DB
func AddItem(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		http.Error(w, "Plase send a request body", http.StatusBadRequest)
		return
	}

	item := models.Item{}
	if err := json.NewDecoder(r.Body).Decode(&item); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	database := r.Context().Value("db").(*mgo.Database)
	if err := db.InsertItem(database, item); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// UpdateItem receives json and updates the required id
func UpdateItem(w http.ResponseWriter, r *http.Request) {
	database := r.Context().Value("db").(*mgo.Database)
	id := chi.URLParam(r, "id")

	item := models.Item{}
	if err := json.NewDecoder(r.Body).Decode(&item); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := db.UpdateItem(database, id, item); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// DeleteItem receives id and then deletes the document in mongo
func DeleteItem(w http.ResponseWriter, r *http.Request) {
	database := r.Context().Value("db").(*mgo.Database)
	id := chi.URLParam(r, "id")

	if err := db.DeleteItem(database, id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// Ping для теста
func Ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("pong\n"))
}

// AddOption add option to id
func AddOption(w http.ResponseWriter, r *http.Request) {
	database := r.Context().Value("db").(*mgo.Database)
	id := chi.URLParam(r, "id")

	err := db.PushOption(database, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// RemoveOption remove option in id item by index
func RemoveOption(w http.ResponseWriter, r *http.Request) {
	database := r.Context().Value("db").(*mgo.Database)
	id := chi.URLParam(r, "id")
	index := chi.URLParam(r, "index")

	if err := db.RemoveOption(database, id, index); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

//
func UpdateOption(w http.ResponseWriter, r *http.Request) {
	database := r.Context().Value("db").(*mgo.Database)
	id := chi.URLParam(r, "id")
	index := chi.URLParam(r, "index")
	option := models.Option{}

	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := db.UpdateOption(database, id, index, option); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// InsertEmptyItem basic item
func InsertEmptyItem(w http.ResponseWriter, r *http.Request) {
	database := r.Context().Value("db").(*mgo.Database)

	if err := db.InsertEmptyItem(database); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
