package middlewares

import (
	"context"
	"log"
	"net/http"

	"github.com/antonkri97/vapeserver/jwt"
	"github.com/rs/cors"

	mgo "gopkg.in/mgo.v2"
)

// Connect передает сессию бд
func Connect(next http.Handler) http.Handler {
	session, err := mgo.Dial("mongodb://192.168.99.100:27017")
	if err != nil {
		panic(err)
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s := session.Clone()
		defer s.Close()

		ctx := context.WithValue(r.Context(), "db", s.DB("vape"))
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// AuthRequired проверяет валиднодсть токена
func AuthRequired(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t := r.Header.Get("Authorization")
		log.Println(t)

		if l := len(t); l == 0 || l > 130 {
			http.Error(w, "bad length", http.StatusUnauthorized)
			return
		}

		if err := jwt.ValidateToken(t); err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func Cors() func(http.Handler) http.Handler {
	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	return cors.Handler
}
