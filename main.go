package main

import (
	"net/http"

	"github.com/antonkri97/vapeserver/handlers"
	"github.com/antonkri97/vapeserver/middlewares"

	"github.com/pressly/chi"
	"github.com/pressly/chi/middleware"
)

func main() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middlewares.Connect)
	r.Use(middlewares.Cors())

	r.Get("/ping", handlers.Ping)
	r.Get("/items", handlers.GetItems)

	r.Post("/login", handlers.Login)

	r.Route("/private", func(r chi.Router) {
		r.Use(middlewares.AuthRequired)

		r.Post("/item", handlers.AddItem)
		r.Post("/item/empty", handlers.InsertEmptyItem)
		r.Put("/item/:id", handlers.UpdateItem)
		r.Delete("/item/:id", handlers.DeleteItem)
		r.Put("/item/option/:id", handlers.AddOption)
		r.Delete("/item/option/:id/:index", handlers.RemoveOption)
		r.Put("/item/option/:id/:index", handlers.UpdateOption)
	})

	http.ListenAndServe(":3000", r)
}
