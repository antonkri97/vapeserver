package db

import (
	"encoding/json"

	"github.com/antonkri97/vapeserver/models"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// GetItems find all items in db
// returns json
func GetItems(database *mgo.Database) ([]byte, error) {
	c := database.C("items")
	items := []models.Item{}

	err := c.Find(nil).All(&items)
	if err != nil {
		return nil, err
	}

	js, err := json.Marshal(items)
	if err != nil {
		return nil, err
	}
	return js, nil
}

// InsertItem вставляет жидкость в бд
func InsertItem(database *mgo.Database, item models.Item) error {
	c := database.C("items")
	if err := c.Insert(item); err != nil {
		return err
	}
	return nil
}

// UpdateItem receives database pointer, id and item
// and then updates the document in mongo
func UpdateItem(database *mgo.Database, id string, item models.Item) error {
	c := database.C("items")

	if err := c.UpdateId(bson.ObjectIdHex(id), item); err != nil {
		return err
	}
	return nil
}

// DeleteItem receives database poiner and id
// then deletes the document from mongo
func DeleteItem(database *mgo.Database, id string) error {
	c := database.C("items")

	if err := c.RemoveId(bson.ObjectIdHex(id)); err != nil {
		return err
	}
	return nil
}

// ValidateAdmin сhecks for login and password
func ValidateAdmin(database *mgo.Database, login string, password []byte) error {
	c := database.C("admin")
	admin := models.Admin{}

	err := c.Find(bson.M{"login": login}).One(&admin)
	if err != nil {
		return err
	}

	err = bcrypt.CompareHashAndPassword(admin.HashedPassword, password)
	if err != nil {
		return err
	}

	return nil
}

// PushOption pushed new empty option to id
func PushOption(database *mgo.Database, id string) error {
	c := database.C("items")

	query := bson.D{{"$push", bson.D{{"options", models.Option{}}}}}

	err := c.UpdateId(bson.ObjectIdHex(id), query)

	if err != nil {
		return err
	}

	return nil
}

// RemoveOption removes item's option by index
func RemoveOption(database *mgo.Database, id, index string) error {
	c := database.C("items")
	objectID := bson.ObjectIdHex(id)
	unset := bson.D{{"$unset", bson.D{{"options." + index, "1"}}}}
	pull := bson.D{{"$pull", bson.D{{"options", nil}}}}

	if err := c.UpdateId(objectID, unset); err != nil {
		return err
	}

	if err := c.UpdateId(objectID, pull); err != nil {
		return err
	}

	return nil
}

func UpdateOption(database *mgo.Database, id, index string, option models.Option) error {
	c := database.C("items")
	objectID := bson.ObjectIdHex(id)
	set := bson.D{{"$set", bson.D{{"options." + index, option}}}}

	if err := c.UpdateId(objectID, set); err != nil {
		return err
	}

	return nil
}

// InsertEmptyItem insert basic item
func InsertEmptyItem(database *mgo.Database) error {
	c := database.C("items")
	item := models.Item{}

	if err := c.Insert(item); err != nil {
		return err
	}

	return nil
}
