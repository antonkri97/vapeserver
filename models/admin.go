package models

// Admin struct for admin collection in mongo
type Admin struct {
	Login          string `bson:"login" json:"login"`
	HashedPassword []byte `bson:"password" json:"password"`
}
