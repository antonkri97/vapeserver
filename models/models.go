package models

import (
	"gopkg.in/mgo.v2/bson"
)

// Item жидкость
type Item struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Name        string        `bson:"name" json:"name"`
	Vendor      string        `bson:"vendor" json:"vendor"`
	Description string        `bson:"description" json:"description"`
	Options     []Option      `bson:"options" json:"options"`
	MediumPhoto string        `bson:"mediumPhoto" json:"mediumPhoto"`
	LargePhoto  string        `bson:"largePhoto" json:"largePhoto"`
}

// Option опция жидкости
type Option struct {
	Capacity float32 `bson:"capacity" json:"capacity"`
	Nicotine float32 `bson:"nicotine" json:"nicotine"`
	Price    float32 `bson:"price" json:"price"`
	Quantity int     `bson:"quantity" json:"quantity"`
}
